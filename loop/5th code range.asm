.MODEL SMALL
.STACK 100H
.DATA 
    star dw "in range$"
    staro db "out of range$"
.CODE

MAIN PROC
MOV AX,@DATA
    MOV DS,AX
                    
    MOV AH,1
    INT 21H
    MOV BL,AL
    
    SUB BL,48
    CMP BL,7
    JG NEXT1
    CMP BL,4
    JL NEXT1

NEXT:
    MOV AH,9
    MOV DX,star
    INT 21H
    JMP NEXT2
    
    NEXT1:
    MOV AH,9
    MOV DX,OFFSET staro
    INT 21H
    
    NEXT2:
EXIT:
MOV AH,4CH
    INT 21H
    MAIN ENDP
END MAIN
